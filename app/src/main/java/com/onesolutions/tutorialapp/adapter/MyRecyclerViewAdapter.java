package com.onesolutions.tutorialapp.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.onesolutions.tutorialapp.R;
import com.onesolutions.tutorialapp.app.App;
import com.onesolutions.tutorialapp.model.GridModel;

import java.util.ArrayList;

/**
 * Created by lenovo on 26-Jan-18.
 */

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder>
{
    private ArrayList<GridModel> arrayList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;

    public MyRecyclerViewAdapter(Context context, ArrayList<GridModel> arrayList, ItemClickListener clickListener)
    {
        mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.arrayList = arrayList;
        mClickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = mInflater.inflate(R.layout.layout_grid_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        GridModel model = arrayList.get(position);
        holder.myTextView.setText(model.getSubject());

        holder.myTextView.setTypeface(App.getUbuntuTypeFace());

        holder.card.setForeground(getSelectedItemDrawable());
        holder.card.setClickable(true);

        holder.card.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                mClickListener.onItemClick(position);
            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView myTextView;
        CardView card;

        ViewHolder(View itemView)
        {
            super(itemView);
            myTextView = (TextView) itemView.findViewById(R.id.info_text);
            card = (CardView) itemView.findViewById(R.id.card);

        }


    }

    public interface ItemClickListener
    {
        void onItemClick(int position);
    }


    public Drawable getSelectedItemDrawable()
    {
        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray ta = mContext.obtainStyledAttributes(attrs);
        Drawable selectedItemDrawable = ta.getDrawable(0);
        ta.recycle();
        return selectedItemDrawable;
    }
}