package com.onesolutions.tutorialapp.model;

import java.util.ArrayList;

/**
 * Created by lenovo on 26-Jan-18.
 */

public class ResponseModel
{
    private ArrayList<DataModel> data;

    public ResponseModel()
    {
    }

    public ArrayList<DataModel> getData()
    {
        return data;
    }

    public void setData(ArrayList<DataModel> data)
    {
        this.data = data;
    }
}
