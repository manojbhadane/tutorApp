package com.onesolutions.tutorialapp.model;

import java.util.ArrayList;

/**
 * Created by lenovo on 26-Jan-18.
 */

public class ResponseGetSubjectModel
{
    private ArrayList<GridModel> data;

    public ResponseGetSubjectModel()
    {
    }

    public ArrayList<GridModel> getData()
    {
        return data;
    }

    public void setData(ArrayList<GridModel> data)
    {
        this.data = data;
    }
}
