package com.onesolutions.tutorialapp.model;

/**
 * Created by lenovo on 26-Jan-18.
 */

public class DataModel
{
    private String sno;
    private String subject;
    private String qno;
    private String qtext;
    private String option1;
    private String option2;
    private String option3;
    private String option4;
    private String ans;

    public String getSno()
    {
        return sno;
    }

    public void setSno(String sno)
    {
        this.sno = sno;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getQno()
    {
        return qno;
    }

    public void setQno(String qno)
    {
        this.qno = qno;
    }

    public String getQtext()
    {
        return qtext;
    }

    public void setQtext(String qtext)
    {
        this.qtext = qtext;
    }

    public String getOption1()
    {
        return option1;
    }

    public void setOption1(String option1)
    {
        this.option1 = option1;
    }

    public String getOption2()
    {
        return option2;
    }

    public void setOption2(String option2)
    {
        this.option2 = option2;
    }

    public String getOption3()
    {
        return option3;
    }

    public void setOption3(String option3)
    {
        this.option3 = option3;
    }

    public String getOption4()
    {
        return option4;
    }

    public void setOption4(String option4)
    {
        this.option4 = option4;
    }

    public String getAns()
    {
        return ans;
    }

    public void setAns(String ans)
    {
        this.ans = ans;
    }
}
