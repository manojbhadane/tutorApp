package com.onesolutions.tutorialapp.model;

/**
 * Created by lenovo on 26-Jan-18.
 */

public class GridModel
{
    private String subject;

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }
}
