package com.onesolutions.tutorialapp.activity.dashboard;

import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.onesolutions.tutorialapp.NavDrawer.DrawerCallbacks;
import com.onesolutions.tutorialapp.NavDrawer.Nav_DrawerFragment;
import com.onesolutions.tutorialapp.R;
import com.onesolutions.tutorialapp.activity.base.BaseActivity;
import com.onesolutions.tutorialapp.adapter.MyRecyclerViewAdapter;
import com.onesolutions.tutorialapp.api.JsonRequest;
import com.onesolutions.tutorialapp.app.App;
import com.onesolutions.tutorialapp.app.Constant;
import com.onesolutions.tutorialapp.databinding.ActivityDashboardBinding;
import com.onesolutions.tutorialapp.listener.ApiResponceListener;
import com.onesolutions.tutorialapp.model.GridModel;
import com.onesolutions.tutorialapp.model.ResponseGetSubjectModel;
import com.onesolutions.tutorialapp.model.ResponseModel;
import com.onesolutions.tutorialapp.utils.ConnectionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lenovo on 26-Jan-18.
 */

public class DashboardActivity extends BaseActivity implements DrawerCallbacks
{
    ActivityDashboardBinding mBinding;
    private ArrayList<GridModel> arrayList;
    MyRecyclerViewAdapter adapter;

    private Nav_DrawerFragment mNavigationNavDrawerFragment;

    @Override public int getLayoutResId()
    {
        return R.layout.activity_dashboard;
    }

    @Override public void init(ViewDataBinding dataBinding)
    {
        mBinding = (ActivityDashboardBinding) dataBinding;

        setToolbarTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        bindList();

        mNavigationNavDrawerFragment = (Nav_DrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
        mNavigationNavDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);


    }

    @Override public void setExtrafont()
    {

    }

    private void bindList()
    {
        arrayList = new ArrayList<>();

        mBinding.prgbar.setVisibility(View.GONE);
//        mBinding.list.setVisibility(View.GONE);


        /*GridModel model = new GridModel();
        model.setSubject(Constant.SUBJECT_PHYSICS);
        GridModel model1 = new GridModel();
        model1.setSubject(Constant.SUBJECT_CHEMISTRY);
        GridModel model2 = new GridModel();
        model2.setSubject(Constant.SUBJECT_MATH);
        arrayList.add(model);
        arrayList.add(model1);
        arrayList.add(model2);*/


        int numberOfColumns = 2;

        mBinding.list.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        adapter = new MyRecyclerViewAdapter(this, arrayList, new MyRecyclerViewAdapter.ItemClickListener()
        {
            @Override public void onItemClick(int position)
            {
                if (ConnectionManager.getInstance(App.getInstance()).isConnectingToInternet())
                {
                    Intent intent = new Intent(DashboardActivity.this, QuesationActivity.class);
                    intent.putExtra(Constant.INTENT_TITLE, arrayList.get(position).getSubject());
                    startActivity(intent);
                } else
                {
                    Toast.makeText(DashboardActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mBinding.list.setAdapter(adapter);

        Log.e("-----", get("aaabbc", 2));//aaac
        Log.e("-----", get("aaabbbc", 3));//c
        Log.e("-----", get("aaabbbc", 1));//aaabbb

        getSubjects();
    }

    // aaabbc,2
    private String get(String str, int no)
    {
        int count = 1;
        char t = str.charAt(0);
        String temp = "";
        String temp1 = str;
        for (int i = 1; i < str.length(); i++)
        {
            if (t == str.charAt(i))
            {
                count++;
                temp = temp + str.charAt(i);
            } else
            {
                if (count == no)
                {
                    temp1.replace(temp, "");
                }
            }
        }

        return temp1;
    }

    // aaabbc,2
    private String gset(String input, int no)
    {
        String ttt = "";
        int count = 1;
        String rrr = input;
        char temp = input.charAt(0);
        for (int i = 1; i < input.length(); i++)
        {
            if (temp == input.charAt(i))
            {
                ttt = ttt + input.charAt(i);
                count++;

            } else
            {
                if (count == no)
                {
                    rrr.replace(ttt, "");
                }

                count = 0;
            }
        }
        return rrr;
    }

    private void getSubjects()
    {
        if (ConnectionManager.getInstance(App.getInstance()).isConnectingToInternet())
        {
            JsonRequest.getInstance().makeRequest(Constant.URL_GET_SUBJECTS, ResponseGetSubjectModel.class, new ApiResponceListener()
            {
                @Override
                public void onResponce(Object aClass)
                {
                    ResponseGetSubjectModel model = (ResponseGetSubjectModel) aClass;
                    arrayList.addAll(model.getData());
                    adapter.notifyDataSetChanged();
                    if (arrayList.size() > 0)
                    {
                        mBinding.prgbar.setVisibility(View.GONE);
                        mBinding.list.setVisibility(View.VISIBLE);
                    } else
                    {
                        Toast.makeText(DashboardActivity.this, "No subjects found", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });

        } else
        {
            Toast.makeText(DashboardActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override public void onNavigationDrawerItemSelected(int position)
    {
        switch (position)
        {
            case 0:
//                startActivity(new Intent(DashboardActivity.this,ComingSoonActivity.class));
                break;
            case 1:
                openUrl();
//                startActivity(new Intent(DashboardActivity.this,ComingSoonActivity.class));
                break;
            case 2:
                startActivity(new Intent(DashboardActivity.this,ComingSoonActivity.class));
                break;
            case 3:
                finish();
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            default:
        }
    }

    private void openUrl(){

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://onesolutions.co.in/privacy/tutorprivacypolicy.html"));
        startActivity(intent);

    }
}
