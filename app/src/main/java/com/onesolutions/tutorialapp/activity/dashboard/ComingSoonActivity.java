package com.onesolutions.tutorialapp.activity.dashboard;

import android.databinding.ViewDataBinding;
import android.view.View;

import com.onesolutions.tutorialapp.R;
import com.onesolutions.tutorialapp.activity.base.BaseActivity;
import com.onesolutions.tutorialapp.app.Constant;
import com.onesolutions.tutorialapp.databinding.ActivityComingSoonBinding;
import com.onesolutions.tutorialapp.databinding.ActivityTestCompleteBinding;

/**
 * Created by lenovo on 26-Jan-18.
 */

public class ComingSoonActivity extends BaseActivity
{
    private ActivityComingSoonBinding mBinding;

    @Override public int getLayoutResId()
    {
        return R.layout.activity_coming_soon;
    }

    @Override public void init(ViewDataBinding dataBinding)
    {
        mBinding = (ActivityComingSoonBinding) dataBinding;

        showToolbarBackBtn(true);
        setToolbarTitle("Coming soon");
    }

    @Override public void setExtrafont()
    {

    }
}
