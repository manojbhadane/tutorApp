package com.onesolutions.tutorialapp.activity.dashboard;

import android.databinding.ViewDataBinding;
import android.view.View;

import com.onesolutions.tutorialapp.R;
import com.onesolutions.tutorialapp.activity.base.BaseActivity;
import com.onesolutions.tutorialapp.app.App;
import com.onesolutions.tutorialapp.app.Constant;
import com.onesolutions.tutorialapp.databinding.ActivityTestCompleteBinding;

/**
 * Created by lenovo on 26-Jan-18.
 */

public class TestCompleteActivity extends BaseActivity
{
    private ActivityTestCompleteBinding mBinding;

    @Override public int getLayoutResId()
    {
        return R.layout.activity_test_complete;
    }

    @Override public void init(ViewDataBinding dataBinding)
    {
        mBinding = (ActivityTestCompleteBinding) dataBinding;

        showToolbarBackBtn(true);
        setToolbarTitle("Test Complete");

        mBinding.txtScore.setText("Score\n" + getIntent().getStringExtra(Constant.INTENT_SCORE));
        mBinding.btnDashboard.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                finish();
            }
        });
    }

    @Override public void setExtrafont()
    {
        mBinding.txtScore.setTypeface(App.getUbuntuRegularTypeFace());
    }
}
