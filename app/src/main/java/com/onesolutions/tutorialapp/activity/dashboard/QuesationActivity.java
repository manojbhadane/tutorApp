package com.onesolutions.tutorialapp.activity.dashboard;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.onesolutions.tutorialapp.R;
import com.onesolutions.tutorialapp.activity.base.BaseActivity;
import com.onesolutions.tutorialapp.app.App;
import com.onesolutions.tutorialapp.app.Constant;
import com.onesolutions.tutorialapp.databinding.ActivityQuesationBinding;
import com.onesolutions.tutorialapp.model.DataModel;
import com.onesolutions.tutorialapp.model.ResponseModel;
import com.onesolutions.tutorialapp.network.MakeRequest;
import com.onesolutions.tutorialapp.network.ResponseListener;
import com.onesolutions.tutorialapp.network.RetrofitApiClient;
import com.onesolutions.tutorialapp.utils.ConnectionManager;
import com.onesolutions.tutorialapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;

/**
 * Created by lenovo on 26-Jan-18.
 */

public class QuesationActivity extends BaseActivity {
    int selected = 0;
    private ArrayList<DataModel> arrayList;
    private ActivityQuesationBinding mBinding;
    private int TotalQue, CurrentQue, mAns, mCorrentAns = 0;
    private DataModel CurrentDataModel;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_quesation;
    }

    @Override
    public void init(ViewDataBinding dataBinding) {
        mBinding = (ActivityQuesationBinding) dataBinding;

        showToolbarBackBtn(true);
        setToolbarTitle(getIntent().getStringExtra(Constant.INTENT_TITLE));

        mBinding.prgbar.setVisibility(View.VISIBLE);
        mBinding.main.setVisibility(View.GONE);

        getData();

        mBinding.rbtGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                mBinding.btnNext.setVisibility(View.VISIBLE);
                mBinding.txtAns.setVisibility(View.VISIBLE);
                mBinding.txtMsg.setVisibility(View.VISIBLE);

                if (radioGroup.getCheckedRadioButtonId() == R.id.rbt1) selected = 1;
                if (radioGroup.getCheckedRadioButtonId() == R.id.rbt2) selected = 2;
                if (radioGroup.getCheckedRadioButtonId() == R.id.rbt3) selected = 3;
                if (radioGroup.getCheckedRadioButtonId() == R.id.rbt4) selected = 4;
                if (selected == 1 || selected == 2 || selected == 3 || selected == 4) {
                    if (mAns == selected) {
                        Utils.printLog("iiiiii-------  " + mCorrentAns);
                        mBinding.txtMsg.setText("Congrats your answer is correct..");
                        mBinding.txtMsg.setTextColor(getResources().getColor(R.color.green));
//                        Toast.makeText(QuesationActivity.this, "Congrats your answer is correct..", Toast.LENGTH_SHORT).show();
                    } else {
                        mBinding.txtMsg.setText("Opps.. wrong answer");
                        mBinding.txtMsg.setTextColor(getResources().getColor(R.color.red));
//                        Toast.makeText(QuesationActivity.this, "Opps.. wrong answer", Toast.LENGTH_SHORT).show();
                    }
                }

                for (int j = 0; j < radioGroup.getChildCount(); j++) {
                    ((RadioButton) radioGroup.getChildAt(j)).setEnabled(false);
                }
            }
        });

        mBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mAns == selected)
                    mCorrentAns = mCorrentAns + 1;
                if (CurrentQue < (TotalQue - 1)) {
                    CurrentQue = CurrentQue + 1;
                    setData(arrayList.get(CurrentQue));
                } else {
                    Intent intent = new Intent(QuesationActivity.this, TestCompleteActivity.class);
                    intent.putExtra(Constant.INTENT_SCORE, mCorrentAns + " / " + TotalQue);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Override
    public void setExtrafont() {
        mBinding.que.setTypeface(App.getUbuntuRegularTypeFace());
        mBinding.rbt1.setTypeface(App.getUbuntuRegularTypeFace());
        mBinding.rbt2.setTypeface(App.getUbuntuRegularTypeFace());
        mBinding.rbt3.setTypeface(App.getUbuntuRegularTypeFace());
        mBinding.rbt4.setTypeface(App.getUbuntuRegularTypeFace());
        mBinding.txtMsg.setTypeface(App.getUbuntuRegularTypeFace());
        mBinding.txtAns.setTypeface(App.getUbuntuRegularTypeFace());
    }

    private void getData() {
//        if (ConnectionManager.getInstance(App.getInstance()).isConnectingToInternet()) {
//            String url = "http://www.onesolutions.co.in/PlaynEarn/get_que_data.php?SUBJECT=" + getIntent().getStringExtra(Constant.INTENT_TITLE);
//            JsonRequest.getInstance().makeRequest(url, ResponseGetSubjectModel.class, new ApiResponceListener() {
//                @Override
//                public void onResponce(Object aClass) {
//                    try {
//                        arrayList = new ArrayList<>();
//                        ResponseModel model = (ResponseModel) aClass;
//
//                        arrayList.addAll(model.getData());
//                        if (arrayList.size() > 0) {
//                            mBinding.prgbar.setVisibility(View.GONE);
//                            mBinding.main.setVisibility(View.VISIBLE);
//                            TotalQue = arrayList.size();
//                            CurrentQue = 0;
//                            setData(arrayList.get(0));
////                    for (int i = 0; i < arrayList.size(); i++)
////                    {
////                        Utils.printLog("---------" + arrayList.get(i).getQtext());
////                    }
//                        } else {
//                            Toast.makeText(QuesationActivity.this, "No quesations found", Toast.LENGTH_SHORT).show();
//                            finish();
//                        }
//                    }catch (Exception e){e.printStackTrace();}
//                }
//            });
//
//        } else {
//            Toast.makeText(QuesationActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
//        }

        if (ConnectionManager.getInstance(App.getInstance()).isConnectingToInternet()) {
            HashMap<String, String> params = new HashMap<>();
            params.put("SUBJECT", getIntent().getStringExtra(Constant.INTENT_TITLE));
//            params.put("SUBJECT", "Physics");

            retrofit2.Call<ResponseBody> call = RetrofitApiClient.getService(this, Constant.URL1).getData(Constant.URL1, params);
            MakeRequest.getInstance().request(call, new ResponseListener() {
                @Override
                public void onResponse(Object object) {
                    String res = (String) object.toString();

                    Log.e("----", "--- " + res);

                    try {
                        arrayList = new ArrayList<>();

                        Gson gson = new GsonBuilder().create();
                        JsonParser jsonParser = new JsonParser();

                        JsonObject jsonResp = jsonParser.parse(res).getAsJsonObject();
                        // Important note: JsonObject is the one from GSON lib!
                        // now you build as you like e.g.
                        ResponseModel model = gson.fromJson(jsonResp, ResponseModel.class);

//                        ResponseModel model = new Gson().fromJson(res,ResponseModel.class);

                        arrayList.addAll(model.getData());
                        if (arrayList.size() > 0) {
                            mBinding.prgbar.setVisibility(View.GONE);
                            mBinding.main.setVisibility(View.VISIBLE);
                            TotalQue = arrayList.size();
                            CurrentQue = 0;
                            setData(arrayList.get(0));
//                    for (int i = 0; i < arrayList.size(); i++)
//                    {
//                        Utils.printLog("---------" + arrayList.get(i).getQtext());
//                    }
                        } else {
                            Toast.makeText(QuesationActivity.this, "No quesations found", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String msg) {

                }

                @Override
                public void showHideProgress(boolean shouldShow) {

                }

                @Override
                public void onAuthFailure() {

                }
            });

//            JsonRequest.getInstance().makeRequest(Constant.URL, params, ResponseModel.class, new ApiResponceListener() {
//                @Override
//                public void onResponce(Object aClass) {
//                    try {
//                        arrayList = new ArrayList<>();
//                        ResponseModel model = (ResponseModel) aClass;
//
//                        arrayList.addAll(model.getData());
//                        if (arrayList.size() > 0) {
//                            mBinding.prgbar.setVisibility(View.GONE);
//                            mBinding.main.setVisibility(View.VISIBLE);
//                            TotalQue = arrayList.size();
//                            CurrentQue = 0;
//                            setData(arrayList.get(0));
////                    for (int i = 0; i < arrayList.size(); i++)
////                    {
////                        Utils.printLog("---------" + arrayList.get(i).getQtext());
////                    }
//                        } else {
//                            Toast.makeText(QuesationActivity.this, "No quesations found", Toast.LENGTH_SHORT).show();
//                            finish();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });

        } else {
            Toast.makeText(QuesationActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void setData(DataModel model) {
        if (CurrentQue == (TotalQue - 1)) {
            mBinding.btnNext.setText("Submit");
        }
        Utils.printLog("Current- " + CurrentQue + " , Total- " + TotalQue);
        CurrentDataModel = model;
        mBinding.rbtGroup.clearCheck();
        for (int j = 0; j < mBinding.rbtGroup.getChildCount(); j++) {
            ((RadioButton) mBinding.rbtGroup.getChildAt(j)).setEnabled(true);
        }
        mBinding.btnNext.setVisibility(View.GONE);
        mBinding.txtAns.setVisibility(View.GONE);
        mBinding.txtMsg.setVisibility(View.GONE);

        mBinding.que.setText("Q." + (CurrentQue + 1) + "\n" + model.getQtext());
        mBinding.rbt1.setText(model.getOption1());
        mBinding.rbt2.setText(model.getOption2());
        mBinding.rbt3.setText(model.getOption3());
        mBinding.rbt4.setText(model.getOption4());
//        mBinding.txtAns.setText("Ans : " + model.getAns());

        if (Integer.parseInt(model.getAns()) == 1)
            mBinding.txtAns.setText("Ans : " + model.getOption1());
        else if (Integer.parseInt(model.getAns()) == 2)
            mBinding.txtAns.setText("Ans : " + model.getOption2());
        else if (Integer.parseInt(model.getAns()) == 3)
            mBinding.txtAns.setText("Ans : " + model.getOption3());
        else if (Integer.parseInt(model.getAns()) == 4)
            mBinding.txtAns.setText("Ans : " + model.getOption4());

        mAns = (Integer.parseInt(model.getAns()));
    }

    private void showDialog(){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Quit");
        builder.setMessage("Are you sure, you want to quit test ??");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                showDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
