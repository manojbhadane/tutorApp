package com.onesolutions.tutorialapp.network;

public interface ResponseListener {
    public void onResponse(Object object);

    public void onError(String msg);

    public void showHideProgress(boolean shouldShow);

    public void onAuthFailure();
}
