package com.onesolutions.tutorialapp.network;

import android.util.Log;

import com.google.gson.Gson;

import com.onesolutions.tutorialapp.BuildConfig;
import com.onesolutions.tutorialapp.app.App;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MakeRequest {

    private static MakeRequest mInstance;

    private MakeRequest() {
    }

    public static synchronized MakeRequest getInstance() {
        if (mInstance == null)
            mInstance = new MakeRequest();
        return mInstance;
    }

    public static void printLog(String log) {
        if (BuildConfig.DEBUG)
            Log.e("----", log);
    }

    /**
     * Returns String as response, later cast it to String
     *
     * @param call     call
     * @param listener result callback
     */
    public void request(Call call, final ResponseListener listener) {
        if (NetworkManager.getInstance().isConnectingToInternet(App.getInstance())) {
            printLog("--Request-- " + call.request().url().toString());
            listener.showHideProgress(true);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == 200 ) {
                            try {
                                listener.showHideProgress(false);
                                listener.onResponse(new JSONObject(response.body().string()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        listener.onError("something went wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("onFailure","--"+t.getMessage()+"\n"+t.getStackTrace()+"\n"+t.getLocalizedMessage()+"\n"+t.getCause());
                    listener.showHideProgress(false);
                    listener.onError("something went wrong");
                }
            });
        } else {
            listener.onError("NETWORK ERROR");
        }
    }

    /**
     * Returns object as response, later cast it based on passed response model
     *
     * @param call          call
     * @param responseModel expected response model
     * @param listener      result callback
     */
    public void request(Call call, final Class<?> responseModel, final ResponseListener listener) {
        if (NetworkManager.getInstance().isConnectingToInternet(App.getInstance())) {
            printLog("--Request-- " + call.request().url().toString());
            listener.showHideProgress(true);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == 200) {

                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                Object model = new Gson().fromJson(jsonObject.toString(), responseModel);
                                listener.showHideProgress(false);
                                listener.onResponse(model);
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onError("something went wrong");
                            }

                        }
                    } else {
                        listener.onError("something went wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    listener.showHideProgress(false);
                    listener.onError("something went wrong");
                }
            });
        } else {
            listener.onError("NETWORK ERROR");
        }
    }

}
