package com.onesolutions.tutorialapp.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by suraj.bokankar on 3/4/17.
 */

public class RetrofitApiClient {


    private static final String TAG = "RetrofitApiClient";
    public static ApiServiceInterface serviceInterface=null;

    public static Retrofit getInstance(final Context context, String baseUrl){
        OkHttpClient okHttpClient=getClient(context);

        Retrofit retrofit=new Retrofit.Builder().baseUrl(baseUrl)
//                .addConverterFactory(JacksonConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient).build();

        return retrofit;
        }



       public static OkHttpClient getClient(final Context context){
            OkHttpClient okHttpClient = null;
           final TrustManager[] trustAllCerts = new TrustManager[] {
                   new X509TrustManager() {
                       @Override
                       public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                       }

                       @Override
                       public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                       }

                       @Override
                       public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                           return new java.security.cert.X509Certificate[]{};
                       }
                   }
           };

           // Install the all-trusting trust manager
           final SSLContext sslContext;
           try {
               sslContext = SSLContext.getInstance("SSL");
               sslContext.init(null, trustAllCerts, new SecureRandom());
               // Create an ssl socket factory with our all-trusting manager
               final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
               OkHttpClient.Builder builder = new OkHttpClient.Builder();
               builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
               builder.hostnameVerifier(new HostnameVerifier() {
                   @Override
                   public boolean verify(String hostname, SSLSession session) {
                       return true;
                   }
               }).addInterceptor(new Interceptor() {
                   @Override
                   public Response intercept(Chain chain) throws IOException {
                       Request original = chain.request();
                       Request.Builder requestBuilder = original.newBuilder();

                       Request request = requestBuilder.build();

                       return chain.proceed(request);
                   }
               });
               List<ConnectionSpec> specList=new ArrayList<>();

               ConnectionSpec connectionSpec=ConnectionSpec.COMPATIBLE_TLS;
               ConnectionSpec connectionSpecOne=ConnectionSpec.CLEARTEXT;
               ConnectionSpec connectionSpecTwo=ConnectionSpec.MODERN_TLS;
               specList.add(connectionSpec);
               specList.add(connectionSpecOne);
               specList.add(connectionSpecTwo);

               HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
               interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


               okHttpClient  = builder
                       .connectTimeout(60, TimeUnit.SECONDS)
                       .writeTimeout(60, TimeUnit.SECONDS)
                       .addInterceptor(interceptor)
                       .readTimeout(60, TimeUnit.SECONDS).connectionSpecs(specList)
                       .build();
               //return okHttpClient;
           } catch (Exception e) {
               e.printStackTrace();
               Log.i(TAG, "getClient: Error="+e.getMessage());
           }
           return okHttpClient;
        }


    public static Retrofit getJsonInstance(final Context context, String baseUrl){
        OkHttpClient okHttpClient=getClient(context);
        Retrofit retrofit=new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(new NullOnEmptyConverterFactory()).addConverterFactory(GsonConverterFactory.create(new Gson())).client(okHttpClient).build();
        return retrofit;
    }



    public static  class NullOnEmptyConverterFactory extends Converter.Factory {

        @Override
        public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
            final Converter<ResponseBody, ?> delegate = retrofit.nextResponseBodyConverter(this, type, annotations);
            return new Converter<ResponseBody, Object>() {
                @Override
                public Object convert(ResponseBody body) throws IOException {
                    Log.i(TAG, "convert: Response"+body.string());
                    if (body.contentLength() == 0) return null;
                    return delegate.convert(body);
                }
            };
        }
    }

    public static ApiServiceInterface getService(Context context, String baseUrl) {
        return getInstance(context,baseUrl).create(ApiServiceInterface.class);
    }

}
