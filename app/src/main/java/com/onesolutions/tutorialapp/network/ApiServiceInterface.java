package com.onesolutions.tutorialapp.network;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by suraj.bokankar on 3/4/17.
 */

public interface ApiServiceInterface {

    @POST
    public Call<ResponseBody> getData(
            @Url String url, @QueryMap HashMap<String, String> map);

}
