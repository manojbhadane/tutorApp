package com.onesolutions.tutorialapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

/**
 * 1. Icon font file name should be always "icomoon.ttf"
 * Created by manoj.bhadane on 08-03-2017.
 */
public class IconView extends TextView
{
    private static final Map<String, Typeface> TYPEFACES = new HashMap<>();
    private static final String TAG = "IconView";
    private static final String FONTFILE = "icomoon.ttf";

    public IconView(Context context)
    {
        super(context);
    }

    public IconView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setFont(context, attrs);
    }

    public IconView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setFont(context, attrs);
    }

    private void setFont(Context context, AttributeSet attrs)
    {
//        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.IconView);
//        String customFont = a.getString(R.styleable.IconView_font);

        setFont(FONTFILE);
//        a.recycle();
    }

    public boolean setFont(String asset)
    {
        Typeface typeface = null;
        try
        {
            typeface = get(asset);
        } catch (Exception e)
        {
            Log.e(TAG, "Could not set TypeFace : " + e.getMessage());
            return false;
        }
        setTypeface(typeface);
        return true;
    }

    public Typeface get(String fontFileName)
    {
        Typeface typeface = TYPEFACES.get(fontFileName);
        if (typeface == null)
        {
            typeface = Typeface.createFromAsset(getContext().getResources().getAssets(), fontFileName);
            TYPEFACES.put(fontFileName, typeface);
        }
        return typeface;
    }
}
