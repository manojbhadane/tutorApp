/*
 * Class : com.itgurussoftware.retailerapp.listener.DrawerCallbacks
 * Author : iT Gurus Software
 * Copyright (C) 2016, iT Gurus Software. All rights reserved.
 */

package com.onesolutions.tutorialapp.NavDrawer;

/**
 * Created by d-codepages on 11/11/2014.
 */
public interface DrawerCallbacks
{
    void onNavigationDrawerItemSelected(int position);
}
