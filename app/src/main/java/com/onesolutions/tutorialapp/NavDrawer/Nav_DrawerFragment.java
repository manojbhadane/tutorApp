
package com.onesolutions.tutorialapp.NavDrawer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.onesolutions.tutorialapp.R;

import java.util.ArrayList;
import java.util.List;

public class Nav_DrawerFragment extends Fragment implements DrawerCallbacks
{
    private DrawerCallbacks mCallbacks;
    private RecyclerView mDrawerList;
    private View mFragmentContainerView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private int mCurrentSelectedPosition;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mDrawerList = (RecyclerView) view.findViewById(R.id.drawerList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mDrawerList.setLayoutManager(layoutManager);
        mDrawerList.setHasFixedSize(true);
        final List<NavigationItem> navigationItems = getMenu();
        Nav_adapter adapter = new Nav_adapter(getActivity(), navigationItems);
        adapter.setNavigationDrawerCallbacks(Nav_DrawerFragment.this);
        mDrawerList.setAdapter(adapter);

//        appDataHelper = AppDataHelper.getInstance(getActivity());

        RelativeLayout lay_profile = (RelativeLayout) view.findViewById(R.id.lay_profile);
        TextView txtUserName = (TextView) view.findViewById(R.id.txtUserName);
        TextView txtUserEmail = (TextView) view.findViewById(R.id.txtUserEmail);

//        txtUserName.setText(appDataHelper.getValueString(AppDataHelper.PREF_NAME));
//        txtUserEmail.setText(appDataHelper.getValueString(AppDataHelper.PREF_EMAIL));

        lay_profile.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                closeDrawer();
//                Intent intent1 = new Intent(getActivity(), AllInOneActivity.class);
//                intent1.putExtra(Constant.INTENT_FRAGMENT_NUMBER, Constant.FRAGMENT_PROFILE);
//                startActivity(intent1);
            }
        });

        selectItem(mCurrentSelectedPosition);
//        selectItem(-1);


        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mCallbacks = (DrawerCallbacks) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }

    }

    public void setup(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar)
    {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
            mActionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close)
        {
            @Override
            public void onDrawerClosed(View drawerView)
            {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) return;
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                hideSoftKeyboard();
                if (!isAdded()) return;
                getActivity().invalidateOptionsMenu();
            }
        };

        mDrawerLayout.post(new Runnable()
        {
            @Override
            public void run()
            {
                mActionBarDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
    }

    public void openDrawer()
    {
        mDrawerLayout.openDrawer(mFragmentContainerView);
    }

    public void closeDrawer()
    {
        mDrawerLayout.closeDrawer(mFragmentContainerView);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mCallbacks = null;
    }

    public List<NavigationItem> getMenu()
    {
        List<NavigationItem> items = new ArrayList<NavigationItem>();

        items.add(new NavigationItem("Home", getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_rocket));
        items.add(new NavigationItem("Privacy policy", getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_info));
        items.add(new NavigationItem("Exit", getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_switch));

//        items.add(new NavigationItem(getResources().getString(R.string.title_current), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_tag));
//        items.add(new NavigationItem(getResources().getString(R.string.title_hindi), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_tag));
//        items.add(new NavigationItem(getResources().getString(R.string.title_hindi), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_tag));
//        items.add(new NavigationItem(getResources().getString(R.string.title_current), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_tag));
//        items.add(new NavigationItem(getResources().getString(R.string.title_hindi), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_tag));
//        items.add(new NavigationItem(getResources().getString(R.string.title_hindi), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_tag));
//        items.add(new NavigationItem(getResources().getString(R.string.title_hindi), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_tag));
//        items.add(new NavigationItem(getResources().getString(R.string.title_manage_subretailers), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_sub_retailers));
//        items.add(new NavigationItem(getResources().getString(R.string.title_qrcode), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_qr_code));
//        items.add(new NavigationItem(getResources().getString(R.string.title_change_pin), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_change_pin));
//        items.add(new NavigationItem(getResources().getString(R.string.title_logout), getResources().getDrawable(R.drawable.abc_btn_radio_material), R.string.icon_log_out));

        return items;
    }

    void selectItem(int position)
    {
        mCurrentSelectedPosition = position;
        if (mDrawerLayout != null)
        {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null)
        {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }

//        ((Nav_adapter) mDrawerList.getAdapter()).selectPosition(position);
        ((Nav_adapter) mDrawerList.getAdapter()).selectPosition(0);

    }

    public boolean isDrawerOpen()
    {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position)
    {
        selectItem(position);
    }

    public DrawerLayout getDrawerLayout()
    {
        return mDrawerLayout;
    }

    public void setDrawerLayout(DrawerLayout drawerLayout)
    {
        mDrawerLayout = drawerLayout;
    }

    public void hideSoftKeyboard()
    {
        if (getActivity().getCurrentFocus() != null)
        {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

}
