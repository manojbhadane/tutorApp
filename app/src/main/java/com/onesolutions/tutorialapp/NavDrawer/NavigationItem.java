/*
 * Class : com.itgurussoftware.retailerapp.models.NavigationItem
 * Author : iT Gurus Software
 * Copyright (C) 2016, iT Gurus Software. All rights reserved.
 */

package com.onesolutions.tutorialapp.NavDrawer;

import android.graphics.drawable.Drawable;

/**
 * Created by d-codepages on 11/11/2014.
 */
public class NavigationItem
{
    private String mText;
    private int mIcon;
    private Drawable mDrawable;

    public NavigationItem(String text, Drawable drawable, int icon)
    {
        mText = text;
        mDrawable = drawable;
        mIcon = icon;
    }

    public String getText()
    {
        return mText;
    }

    public void setText(String text)
    {
        mText = text;
    }

    public Drawable getDrawable()
    {
        return mDrawable;
    }

    public void setDrawable(Drawable drawable)
    {
        mDrawable = drawable;
    }

    public int getmIcon()
    {
        return mIcon;
    }

    public void setmIcon(int mIcon)
    {
        this.mIcon = mIcon;
    }
}
