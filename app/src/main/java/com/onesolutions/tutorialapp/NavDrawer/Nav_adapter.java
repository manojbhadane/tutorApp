/*
 * Class : com.itgurussoftware.retailerapp.adapters.Nav_adapter
 * Author : iT Gurus Software
 * Copyright (C) 2016, iT Gurus Software. All rights reserved.
 */

package com.onesolutions.tutorialapp.NavDrawer;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.onesolutions.tutorialapp.R;
import com.onesolutions.tutorialapp.app.App;

import java.util.List;

/**
 * Created by d-codepages on 11/11/2014.
 */
public class Nav_adapter extends RecyclerView.Adapter<Nav_adapter.ViewHolder>
{

    private List<NavigationItem> mData;
    private DrawerCallbacks mDrawerCallbacks;
    private int mSelectedPosition;
    private int mTouchedPosition = -1;
    private Context mContext;

    public Nav_adapter(Context context, List<NavigationItem> data)
    {
        mData = data;
        mContext = context;
    }

    public void setNavigationDrawerCallbacks(DrawerCallbacks drawerCallbacks)
    {
        mDrawerCallbacks = drawerCallbacks;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.drawer_row, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i)
    {
        viewHolder.textView.setText(mData.get(i).getText());
        viewHolder.icon.setText(mContext.getResources().getString(mData.get(i).getmIcon()));
//        viewHolder.icon.setTypeface(App.getUbuntuTypeFace());
//        viewHolder.textView.setCompoundDrawablesWithIntrinsicBounds(mData.get(i).getDrawable(), null, null, null);

        viewHolder.textView.setTypeface(App.getUbuntuTypeFace());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener()
                                               {
                                                   @Override
                                                   public void onClick(View v)
                                                   {
                                                       if (mDrawerCallbacks != null)
                                                           mDrawerCallbacks.onNavigationDrawerItemSelected(i);
                                                   }
                                               }
        );

        if (mSelectedPosition == i || mTouchedPosition == i)
        {
            viewHolder.textView.setTextColor(mContext.getResources().getColor(R.color.yellow));
            viewHolder.icon.setTextColor(mContext.getResources().getColor(R.color.yellow));
            viewHolder.bar.setVisibility(View.VISIBLE);
            viewHolder.itemView.setBackgroundColor(viewHolder.itemView.getContext().getResources().getColor(R.color.selection));
        } else
        {
            viewHolder.textView.setTextColor(mContext.getResources().getColor(R.color.white));
            viewHolder.icon.setTextColor(mContext.getResources().getColor(R.color.white));
            viewHolder.bar.setVisibility(View.INVISIBLE);
            viewHolder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    public void selectPosition(int position)
    {
        int lastPosition = mSelectedPosition;
        mSelectedPosition = position;
        notifyItemChanged(lastPosition);
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount()
    {
        return mData != null ? mData.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView textView, icon;
        public View bar;

        public ViewHolder(View itemView)
        {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.item_name);
            icon = (TextView) itemView.findViewById(R.id.icon);
            bar = (View) itemView.findViewById(R.id.bar);
        }
    }
}
