package com.onesolutions.tutorialapp.api;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.onesolutions.tutorialapp.app.App;
import com.onesolutions.tutorialapp.listener.ApiResponceListener;
import com.onesolutions.tutorialapp.utils.Utils;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by manoj.bhadane on 16-12-2017.
 */
public class JsonRequest {
    private static JsonRequest mInstance;
    private Gson mGson;

    private JsonRequest() {
        mGson = new Gson();
    }

    public static synchronized JsonRequest getInstance() {
        if (mInstance == null)
            mInstance = new JsonRequest();
        return mInstance;
    }

    public void makeRequest(String url, final Class<?> aClass, final ApiResponceListener listener) {
        try {
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try{
                    Utils.printLog("-RES-" + response.toString());
                    Object model = mGson.fromJson(response.toString(), aClass);
                    listener.onResponce(model);}catch (Exception e){e.printStackTrace();}
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.printLog(error.getMessage().toString());
                }
            });

            Utils.printLog("-RES-" + jsObjRequest.getUrl());

            App.getInstance().addToRequestQueue(jsObjRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void makeRequest(String url, final Map<String, String> jObject, final Class<?> aClass, final ApiResponceListener listner) {
        try {

            Utils.printLog("-jObject-" + jObject.toString());

            StringRequest strReq = new StringRequest(Request.Method.POST,
                    url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        Utils.printLog("-RES-" + response);
                        Object model = mGson.fromJson(response.toString(), aClass);
                        listner.onResponce(model);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                hideProgressDialog();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    listner.onResponce(error.getMessage());
//                Log.e(TAG, "Registration Error: " + error.getMessage());
//                Toast.makeText(mContext,
//                        "Unknown error occured", Toast.LENGTH_LONG).show();
//                hideProgressDialog();`
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    return jObject;
                }
            };

            // Adding request to request queue
            Utils.printLog("-REQ-2" + strReq.getUrl());
            Utils.printLog("-REQ-1" + strReq.getBody());
            Utils.printLog("-REQ-3" + strReq.getHeaders().toString());
            App.getInstance().addToRequestQueue(strReq, "tag_reg");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
